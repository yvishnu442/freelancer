import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View, ScrollView} from 'react-native';
import {Button} from 'react-native-elements';

import Swiper from 'react-native-swiper';

const styles = StyleSheet.create({
  wrapper: {},
  mainView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  accountbutton: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    bottom: 10,
    position: 'absolute',
  },
});

const SwiperComponent = () => {
  return (
    <View style={styles.mainView}>
      <Swiper
        style={styles.wrapper}
        showsButtons={true}
        autoplay={true}
        autoplayTimeout={1}
        dotStyle={{
          marginLeft: 3,
          marginRight: 3,
          marginTop: 60,
          marginBottom: 60,
        }}>
        <View style={styles.slide1}>
          <Text style={styles.text}>Hello Swiper</Text>
        </View>
        <View style={styles.slide2}>
          <Text style={styles.text}>Beautiful</Text>
        </View>
        <View style={styles.slide3}>
          <Text style={styles.text}>And simple</Text>
        </View>
      </Swiper>
      <View style={styles.accountbutton}>
        <Button title="Create an Account"></Button>
        <Text> Already have an account? Log in Here</Text>
      </View>
    </View>
  );
};

export default SwiperComponent;
